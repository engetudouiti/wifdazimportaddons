#[Update log]
blender 2.8 already released as product version. Then I will not up-date this small script for 2.79 anymore.

but the "Daz importer plug-in verison" which Thomas daz importer have pakcaged as plug in, 
seems work as same as before. so if you are 2.79 user. 
(I know it is more stable)  please use Thomas DAZ importer packaged one if you need.
then you should not downlaod file from here , current file is for 2.8 up only. 
------------------------

This small script was made to add some functions which I have used with Daz Importer (by **Thomas Larsson**)

To use this script, first download and install recent verison of **Daz Importer"** from Thomas repository please.
https://bitbucket.org/Diffeomorphic/import-daz/src/default/
 
general idea is brought from  **"Darren Payson"** who show me how G8 bones are generated in blender, and show clear pic which change local axis in blender when pose, and offered sample script to edit bone tips. From these I could get idea to adjust bone roll and tips from json.

full document of DAZ importer is here (by Thomas)
[Thomas DAZ importer blog](https://diffeomorphic.blogspot.com/p/daz-importer-version-15.html)

if you are 2.79 user, just keep the one which DAZ importer have packaged in "add on" directory, so do not download from here.

if you are 2.80 + user, and hope to test some new functions, download **"wifDazBoneUtility.py"**
from here. install it in your blender/scripts/addons directory.   as same as other blender add on.

Then up-date ,remove, 'acttivate" 'de-activate' from blender user preferecne.

===========================
Main function 

It adjust defaut import rig bone local axis (roll) and tip position as same as daz studio rigs.

When you pose bones which imported by Daz importer, with set local axis in blender, you may see current default rig is somehow different from which we see in daz studio, and their bone tip location is slightly different from daz studio. 
About some figure it is not small difference. (eg genesis8)

This add on main function (adjust daz bones) try to adjust "DAZ importer default rig" bone tip location and roll of all bones to get almost same visual infromaton of local axis direction which we see in daz to pose easy.

And adjust bone tips correctly. (it is more important I think)

(all bone rotation axis need to change as YZX in blender, eg for twist use local Y axis, and suppose X axis for main pose , because blender generate bone so. we can not use Z or X as bone roll axis.)

-------------------

#usage

this script work with DAZ importer beta versions. it depend on exported json data, then you must up-date
**"export_basic_data.dsa"** which offered with Thomas daz importer recent beta.

activate script from blender preference ( **"wifDazBoneUtility.py"**)

it add new tab labelled as ** "DAZboneUtility" ** there are some buttons to run each functions.

## a) Change Bone Name

(for blender X-axis mirror pose (And Relative Mirror for blender 2.8) , basically I only made for pose mode.
to play with default rig. 

### 1 select Armature in object or pose mode
### 2 click "change bone name" button

it change bones name of importer default rigs to work for blender mirror pose. 
At same time, blender auto change vertex group name of meshes which attached with current rig by armature modifier.

The rule is simple.When there is "rBone and lBone" in selected rig, it change as "r.Bone and l.Bone" for blender mirror.

Though Thomas add on will auto change bone names about main bones when you convert as MHX or Rigi-fy for Blender mirror,
but about default rig and added facial bones, it keep "daz bone name" (not compatible for blender mirror).

then "change bone name" when you hope to use mirror pose, or copy and flip keys on timeline.

I do not know detail but, blender auto change channell name on time line when you change bone name. 
so it may cause no problem when you change bone name with editting keys on timeline.

After set key, or set pose,  must click **“ Return bone name”**  without it,  a few Daz Importer Add on functions not work any more. We need to keep same bone name to import pose animation file etc.

------------------

## b) Return Bone Name

discribed above, it will return modified bone names as default. select armature which you need to return bone name,
then click "Return Bone Name" 

Whenever you change bone name for mirror pose, then finish your work, (pose or set keys etc) **"return bone name"** please.

(it must need ,and I strongly recommend to use all DAZ importer functions)

And you should not change armature modifier setting for meshes. If you keep armature modifier for each meshes, blender auto change vertex group name correspond to current rig bone name. if you remove armature modifier for some meshes, then return or change armature bone name, those meshes (which remove armature modifier) do not change vertex gorup name. then even though you re-attach armature modifier, meshes will not follow rig posing.

------------------

## (new) setJsonPath

I recommend, when you first import daz scene, with Daz importer, must use this function.
it set ”json file” absolute path, for all improted item (mesh and rig) as new csutom prop "DazAbsJs" for each object(mesh and armature) by using mesh "dazId" which daz importer auto set. (but only for meshes, and the duf is not absolute pass)

without it, you can not find which json file is used to generate each rig and meshes.

you only need it, one time, when you import daz scene (duf) with json.

to use it 

###1 select objects which you need to set absolute path. (select mesh and rigs) by "setJsonPath"

###2 click "setJsonPath" button.

it attach absolute json path for each mesh, at same time, atach same props for parent rig. 
so if in the scene there is only rig,or the rig do not have any mesh as child, the rig can not set absolute path.

Without mesh object, there is no way, to get absolute json path.  
(daz importer not record "json" path infomation for rig)

In console window, you may see, which obj (mesh and parent armature) set "absolute json path" which used to import.
basically, you may only import one duf and json, pair. but even though you import multi scene (duf and json)
then select all >> setJsonPath may work. but I do not recommend it.

I recommend, after import "one duf and json", soon you  select all meshes and rigs which daz importer generated.
it may locate in same collections.  then set absolute path for them, by this button. 
 
once you did so, you can find which json are used for each object and rig forever. to know it, just see object> custom prop "DazAbsJs" of each obj, in property panell. 

And once script attach "DazAbsPath" and value, for object, even though you re-use the "setJsonPath",  the object never change
 the value (script simply ignore, when the obj have "DazAbsPath" as custom prop)  so you need not worry if you click again and again. (only first time it work for each object)

but if you import scene , then change mesh parent rig,(though it seldom happen,,but may happen import mulit figure etc,,)
 
and try to "setJsonpath", with select rig and meshes, about mesh it is ok. (because it already have relative path by daz importer)
but about rig, it simply copy current child mesh absolute path. so rig may record different json path, what you really import. 

(I often forget duf and json file path, which I imported to each blender scene, then made it)
usually we only need to "setJsonPath" first time. when you import another duf with json,
and mix with current blender scene, everytime you can use this button, to set json (duf) path, which you imported.

## c) import json

#Caution!  you should not use "import json" after "merge rigs". 

because after merge rigs, added rig bones will loose custom props which will be used to adjust rigs.

it need for **Adust Daz bones**  and it may confuse users, then hope to discribe why it need and what will happen.

The "json file" is what you had saved in daz studio to import daz scene in blender, with same name duf. 
And you need to use "import json" untill merge rigs by Daz Importer. 

After you import daz scene(duf) by DAZ importer, select all mesh and obj (which daz importer imported)
>>click "setJsonPath" first. so it set json absolute path for each obj (mesh and rig) as new csutom prop.

Then only select rigs, (though script may handle, when you select mesh too, it just skip over)
click "Import Json" untill import morph or controller etc.

summarize work flow,

import daz scene duf (with json) >> set absolute json path for each rig and meshes  

###1 select only rigs(armatures) from out-liner in object mode

###2 click "import json" button

as mentioned above, if you "setJsonPath" already,  
blender file exploler will open the "directory" and auto set "filepath" your "json" which used for those rigs.

You should not select multi rigs which imported by different json.  usually it never happen.
eg you can not import json, for rigs which improted by 2 duf etc.. but you can import json, for all rigs which improted by the json.
to make it simple, when you import duf scene, I recommend save it first.

then import json. 

if not open duf (or json) directory, manually set directory and select the json file please.
(but if you did "setJsonPath", after you load duf (and json), it should set correct json file, which used to import
rig and mesh. 

###3 confirm the **json** which used to import current scene is slected,   then click "import" 

This funciton only add some custom properties for each armature edit-bones from json. (it not add any props for mesh)
 
It make no difference as visuall and even though it fail, ( could not find figure in json, which you selected)
it cause no issue. You may see all bones have new custom properties, but it not overwrite other propeties 
added by "Daz Importer" , and it do nothing itself.

when  "Import json", I recommend check console, and message. 
if script work correctly about all selected rigs, you may find all Figure(armature) name 
which you selected, and see "added property" message in console.

If you find some flag [0,1,0] in console , it means the rig can not find data from json.
But about recent version, this script can find all figures which exported by Thomas plug in 
correspond to blender armature name. if it failed, that means you select wrong json. so take care to select
json which you actually used for current scene.(same name as imported duf name)

About recent version of Thomas daz importer update export_json script. then it can export json
include figure label. and it is same as armatuer name in blender. and I added fucntion to clean up for blender famous
naming sysytem, like  armature.00X when you duplicate rig or meshes. without it script use the obj.name.
then such names never found in json. (though it seldom not happen, but you import multi scene first, it may occur)

 so almost there is no case,   my script fail to find rig in json, and may add new properties , which need to adjust daz bones.

------------------

## d) Adjust Daz bones

Actually this is main funciton of this small scripts.

To work it, you must need **import json** for your selected rigs first. so it can attach propertys for all bone,
which need to re-generate all bones from scratch. 

"Adjust Daz bones​" use these properties values for circulation , which recorded in json.
if the bone do not have the property, it can not be adjusted. so it will be skipped.

I recommend, untill try "adjust Daz bones", 

a) save current scene as blend file then keep it.  so you can return if something wrong.
b) "save rig_preset", which I added for recnet add on. then you can return it when you need by "load rig_preset"

"Adjust Daz bones​" change bone tips location and roll about all bones of rigs currently selected.
if your selected rig bones not have new custom property (by import json)this function will be skipped.

so please remember, 

At first **"import json"** for armature, then you can **"adjust daz bones"** 

once rig bones have new properties by "import json" , you can use "adjust daz bones" when you need it.
but if you merge rigs, it will loose custom property, for bones. so you should use "adjust rigs" untill merge rigs.
(that means, you need to "adjust rigs", for all rig. untill you merge them)

best practice is, select only one rig (eg base actor rig genesis8 etc),with keep bone "local axis" visible
then "import json" >> "adjust Daz bones" ,to see how it work 

it will move bone tip location. and adjust roll(local axis direction). you can clear see difference with genesis8 figure bones. 

If you clear understand, how it work, multi select all rigs, and use "Adjust Daz bones" it may auto adjust all rig and bones.
by one-click. 

Though it is not about this add on usage, but When "adjust Daz Bones" with multi selection rigs, 
It may change current "Active rig" of selection, so do not click Daz Importer >"merge rig" soon.

set active rig as your "base character rig", after that "merge rigs" please (from DAZ importer tab).

I test with rig-fy rig converted by Daz Importer ,  basically I could not find problem which caused by this add on.

Daz Importer will change bones to work with Rigi-fy, (some bones will connect etc)
but it keep roll value about most of bones, which set by this script function, and it will work without problem.

------------------------------

## e) Memorize edit-bones (added with new verison)

At first, once you "import json", this script auto "memorize" default Daz Importer rig bone infomation.

So without you have special reason, you need not use this.  if you use ths button, after "Adjust daz rigs"
or edit bone manually, that means, you memorize your current (which you edit, or adjust by script) rigs bone infomation.
 so it lost default rig bone infomation. you can never return "daz importer rigs" untill you re-import scene.

Basically,  it work as same as "Daz Studio" "memorize figure rigging".

When you select armatures and clcik "memorize edit bones" , it add or update custom props which record
all edit-bone Head, Tip, and orientation (blender roll) values.

I say again. About daz importer rigs, when you "import json", it auto record current selected armature edit bone values as "memorize values". to easy return default rig. 

but it only happen when there is no memorize values. If you have already "memorized edit bones" or you have already "imported json" it not change current memorize value.

On the other hand if you click "Memorize edi-bones", it must record current edit-bones value as default.

That means, if you "memorize edit bones" after "Adjust DAZ bones"  now you edit bone and memorize  them as default value.
when you really need to change default values of edit-bones , to resotre later, use this function.

## f) Restore edit-bones (added with new verison)
it work as same as "DAZ studio" "restore figure rigging".

when you select armatures and click "Restore edit bones", script serch custom props which added by
"memorize edit bones". or auto added by "import json"  if edit bone have no default values , nothing happen.

About Daz Importer Rig, script auto add default value as csutom prop when you "import json" first time for each rig.
that means "import json" do "Memorize edit-bones" of current armature only when there is no default values.

So if you hope to keep daz importer default rig , you only need "import json". 
then only use "Restore edit-bones" when you need. 

or to confirm, after "import json" without edit anything, = all rig keep as same as daz improter generated.
click "memorize figure". then you can use "Restore figure" after "adjust rig" or edit rig manually.

Of course it only restore tip, head, roll about bones.

*Caution!!*
Though I believe you can understand usage,

After you edit bone roll or head, tail location, manually or used "adjust Daz bones", if you click "Memorize edit-bones"

now default memorized value change. then when you "restore edit bones", it return edit bones which you memorized.(aleady editted or adjusted by this script), then not return to daz importer default rig.

------------------------------

## g) Update bone layer

Current 2.8 still have issue, to up-date bone layer dot. for some cases.
eg when you merge rigs with daz importer some bone dot will be not shown in bone layer.
this function force to up-date bone layer, then show dot correctly

When you find some bone seems not show dot in bonelayer, just Select rigs (in object mode) which you need to up-date, then click this button.

It should return all dot in bone layers.

I hope this bug will be removed by blender dvelopers, or there will be officially recommend way to up-date rig
but at current this way work well.

## h)Filp bone

it only need when you edit or customize bone, and it just filp bone direction opositte.(move bone tip to opositte side) in edit mode with keep current head point location. most of case you may not use it .

## i)Plus or minus roll

it only works in armature edit mode ,and selected bones. (you can select multi bones)
it just add 90 roll or minus 90 roll from current values when you click these buttons.

even though import json and use daz orientation, it not means we get perfectly same 3 eular direction about each axis.
it is imposibble.  simply because, daz can set bone direction with more variety.(you can set bone roll axis as X,Y,Z
as you like for each bone) but blender bone roll axis is already decided (as visuall) as Y
then we can only roll. and we can not swap X and Z.  

if you test with adjusted rig with my script, but you hope to customize more, you only need to set roll plus or minus pi/2, pi.
or you may try flip bones. it is all what we can do.

then I can not confirm, how set roll direction is best for all type rig. As for me, current setting cause no issue.
but if I hope to get perfect same roll setting as blender meta-rig of rig-fy, I may need to change roll some bones
though I have no issue, after convert daz rig (which adusted my script), if there is case you need to customize,
maybe these 2 buttons reduce your work. or you may think some bone direction is not good to keep daz studio way.

As ideally, make preset for each generation figure, then user import it. after adjust rig.

the data only need to discribe,customized value with figure type, bone name, then modified delta value (pi/2 multipled)
if it really need, I may plan to change it.

after all, I think the best way is, DAZ importer offer option, when import scene duf with json. generate rig
as same as my script adjusted rig status. import json again is not good. (I know it), but there is no way to take all value 
which need to adjust rig correctly.

after it will be done, may be add save or import preset for each generation figure (of figure type)
for user customize.

--------------------------
I add some simple preset system, at curent,it do

a) save and load all morph value (plug in generate as morph controller)
b) save and load, current rig edit-bone parameter (roll,center point, end point) 
so you can return it as you like after you edit rigs. 

## save and load morph preset (json) (beta) _prese.json

I added new 2 functions , to save current daz morph values (plug in generate) as json, then import and apply when we need.
The Thomas blender plug in developed much to import export daz native files. But I really hope to keep each shape, which tweaked in blender, save it as new preset. only for blender scene.

So I have requested it long time, but recently I happend to understand way to gather only daz morphs with prefix.
At current, it save all Rig custom prop which have "Dz" prefix, in ID as json dictionary.  then load it as you like.
(so now you can save, and apply json preset, as same as daz shape preset)

Thomas daz importer not strictly distinguish Pose and shape, then you may not need think difficult.
This function, just save all Daz plug in morph values , or load and apply those value for current Actor rig.

and someone may not understand why we need to select rig, to save body or clothing morph preset,
but DAZ plug in never add custom prop as driver, when import morph and add driver for mesh shape keys. 

it is desigend, we can morph all clothing at same time (they will be transfered, body to clothing )
to aovid poke through etc.  

that means, we controll each shape key of meshes, by Rig custom properties. 
then this simple preset system, just save those rig custom prop (which use "Dz" prefix) 
values. as json. (not check shape key name etc) , and when load it, apply those value> up-date it. 


(how it work)

### 1 select "Armature" in object or pose mode (do not select mesh, you need to select rig)
### 2 click "save" button .

It open blender file browser, and auto-set filepath In same directory which the scene.blend file are saved.
and auto set name like (scene file name)_preset.json.

Of course you can change directory, and file name as you like. so you can save happy.json or angry1.json etc.
I recommend, after you save json, simply open by text editor, then look, what is discribed.

you may easy understand Only Daz morph properties key and value are recorded as json. Save morph preset, as you like.
but at current it can not select morphs which you saved, so it simply save all morph values, which current rig have.
if you use pose controller for body, then save it, those value recorded too. I plan add option, it only work for active
(use check mark) to load. (so you can save and load only about your checked morph values, without change other morph)

### 3 select Armature in object or pose mode
### 4 click "load" button

It open blender file browser, then auto-set path and name. 
Of course if you did not save json preset, as the default name, you should not load it. (or it may only show erroer)
select json, which you hope to apply for current Actor.
And thanks Thomas add prefix for custom morph too, then now it should work for all morphs which imported by daz scripts.

## save rig set (json) (beta) _rigset.json

it save rig preset. which can recode all edit-bones "roll, tip, tail" of current selected rigs as json.
I recommend, save rig set, for one rig. but it can recode multi rigset too (if you hope so)

simply select rig with object mode, and click it, and save json, (basically script auto set name as  rig_se.json , to user easy remember.
I recommend, you make preset directory first. under your blend scene. to save moprh preset, and rig set.
even though they are all json, what recoded in script is pefeclty different, so you need to remember, it is preset for what. 

## load rig set (json) (beta) 
it load rig preset. which you saved. then apply roll tip, tail for current rig edit bones.

simply select rig, and click it, and select json which you saved as rigset.
if you add new bone, after save _rigset.json,  there is no recode about those bones. then they do not change anything.
and of course if you change rig name, or bone name, there is no recode , so they can not load any data.

I recommend, open your saved json data, with your text editor, then see what is recoded. so you never miss use it.
(do not expect, which may load driver etc)

I make it work only when "figure" name is same. so after save rig-preset, if you edit figure name, it do not load rig-set.
to avoid problem, when you change bone name, figure name,  or add rig, save rig-set. so it can load when you need.
but it never return your edit bone name etc..

and you should not use it for different character even though they use same armature object name.(eg genesis 3 charcters)
rig need to adjust for each shape.  so use different rig_set for different shape simply mess up your work.

============================================


2020 5/27
add new function and button "save and load rigset".
it simply save and load rig edit-bones, roll, head, tail as json. with figure name and bone names.

2020 5/22
add transfer props (wip) (at current it only transfer,3 custom props, this script added  for restore rigging.
it only need when merged rig bones lost those infomation, then you happend to return those from original)
and I do not recommend it. 

change way to auto set json path, now the absolute path are stored as each obj custom props (include armature)
so you can check impor json path for each object, as their "csutom prop" = "DazAbsJs" (mesh and armature)

2020 5/21
add new 2 funcitons, to "save and load Daz morph values" as json. (kind of preset system for morphs)

2020 5/17
add new button to get "absolute json file path" with select mesh objects(actor), the path will be used to import json
to set file path and directory, or to find duf and json file path which used to import current select mesh( with rig)
as info.

2019 10/1
change discription, they only work for 2.8.  (for 2.79, Thomas "DAZ importer" still remain as plug in version.
though I can not confirm)

2019 7/24
add button to set edit bone roll plus minus 90 for user customize when user hope it.

2019 7/21
add button to open info, there seems case, when activate add on, panell info not appear.
up-load 2 files for blender add on, and Thomas daz importer add on.

2019 7/20
add infomation label. which will show message for each action. to check erroer.  but usually console offer more detail.

for 2.8 bone laery dot problem, add new button which can up-date all selected armature bone layer. (not change bone layer, but just show dot correctly)

2019 7/16
when import json for adjust rig, add flags , if user select non json file, now console show message.
and add flags when user use old json file,which was exported by old verssion export_basic_data.dsa
console show message, and do nothing.

2019 7/14 
add button "Memorize edit bones" and "Restore edit bones" it work as same as "memorize figure rigging"
and "Restore figure rigging" in daz studio.  (it only memorize Head Tail, and roll of edit bones)
when import json, it auto memorize about rigs which improted by daz importer. see read me how to use it

2019 7/14 
add button which can return bone as default. To use it, you need to import json first.
once you import json, script add porperty which remain current default values. for edit bones.

2019/7/13
add flag check label for old json (by Thomas) packaged in Thomas DAZ importer newest version.

2019/7/12
Now script can serch data by figure label ,then it offer more stablility.
recommend to downlaod and use newest version.

you must need to update Thomas daz importer(beta) and **"export_basic_data.dsa"**
(for daz studio to export json data) too

2019/7/10

update main script, and read me

remove bug, when blender armature name include " "(space)

---------