bl_info = {
    "name": "DAZ rig bone utility",
    "author": "WaitInFuture",
    "version": (2, 0, 4),
    "blender": (2, 80, 0),
    "location": "View3D > Npanel",
    "description": "renameDAZgenesisBone, adjust rig pos for daz importer rig",
    "warning": "testonly",
    "wiki_url": "https://bitbucket.org/engetudouiti/wifdazimportaddons/src/master/",
    "tracker_url": "",
    "category": "Rigging"}
    
""" blender add on version , it work as individual add on, install as same as other blender add ons"""

import bpy
import import_daz

from bpy_extras.io_utils import ImportHelper
from bpy_extras.io_utils import ExportHelper
from bpy.props import StringProperty, BoolProperty, EnumProperty

from mathutils import Vector, Matrix, Quaternion
from math import radians, pi
import json
import os
import pathlib
import bpy.path
import gzip

    
def unsetSceneprop():
    scns = bpy.data.scenes
    for scn in scns:
        if "wif-info" in scn.keys():
            del scn["wif-info"]
    
def sel_act(context, ob):
    ob.select_set(True)
    context.view_layer.objects.active = ob
    
def showInfo(self, context, msg):
    scn = context.scene
    scn["wif-info"] = msg
    self.report({'INFO'}, msg)
    
def update_prop(context, rig):
    loc = rig.location
    rig.location = loc
    
def mode_pose():
    bpy.ops.object.mode_set(mode = 'POSE')
    
def mode_edit():
    bpy.ops.object.mode_set(mode = 'EDIT')

class WIF_OT_addinfo(bpy.types.Operator):
    bl_idname = "dazinfo.reflesh"
    bl_label = "WIF DAZ show info"
    bl_description = "open info labell"
    bl_options = {'UNDO'}
    
    def execute(self, context):
        scn = context.scene
        scn["wif-info"] = "***infomation***"
        return {'FINISHED'}

class WIF_PT_dazBoneUtils(bpy.types.Panel):
    bl_label = "WIF DAZ bone utility"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "DAZ BoneTool"
    
    def draw(self, context):
        layout = self.layout
        row = layout.row(align=False)
        box = row.box()
        box.operator("bonename.change", text="Change Bone Name")
        box.operator("bonename.dazname", text="Return Bone Name")
        row = layout.row(align=False)
        box = row.box()
        box.operator("import_js.setpath", text="Set Json Path")
        box.operator("import_js.dazdata", text="Import json")
        box.operator("dazbone.adjust", text="Adjust Daz bones")
        box.operator("dazbone.memorize", text="Memorize edit-bones")
        box.operator("dazbone.return", text="Restore edit-bones")
        box.operator("dazbone.transferprop", text="transfer customprop")
        row = layout.row(align=False)
        box = row.box()        
        box.operator("dazbone.dotshow", text="Update bone layer")
        box.operator("dazbone.selectchild", text = "Select Child Bones")
        box.operator("dazbone.deselectchild", text = "Deselect Child Bones")
        box.operator("dazbone.flipbone", text="Flip selected edit bones")
        row = layout.row(align=False)
        box = row.box()
        box.label(text = "plus or minus roll")
        row = box.row()
        row.alignment = 'EXPAND'        
        row.operator("dazbone.addroll", text="+45 roll").rfg = True
        row.operator("dazbone.addroll", text="-45 roll").rfg = False
        row = layout.row(align=False)
        box = row.box()
        box.label(text = "save or load morph preset")
        row = box.row()
        row.alignment = 'EXPAND'        
        row.operator("export.morph_preset", text="save")
        row.operator("import.morph_preset", text="load")
        row = layout.row(align=False)
        box = row.box()
        box.label(text = "save or load rig preset")         
        row = box.row()
        row.operator("export.rig_set", text="save rig set")
        row.operator("import.rig_set", text="load rig set")
        row = layout.row(align=False)
        box = row.box()
        box.label(text = "add roll for base figures")         
        row = box.row()
        row.operator("import.roll_set", text="add rolls from json")     
        
        # row.operator("dazinfo.reflesh", text="Open info")
        scn = bpy.context.scene
        row = layout.row(align=False)
        row.label(text = "infomation")
        if "wif-info" in scn.keys():
            row = layout.row(align=False)
            row.prop(scn, '["wif-info"]', text = "")
        
class WIF_OT_changeDazBoneName(bpy.types.Operator):
    bl_idname = "bonename.change"
    bl_label = "text"
    bl_description = "change bones name for blender mirror"
    bl_options = {'UNDO'}

    def execute(self, context):
        bpy.ops.dazinfo.reflesh()
        obj = bpy.context.active_object
        if not obj:
            msg = "select daz armature first!"
            showInfo(self, context, msg)
            return {'FINISHED'}
            
        elif not obj.type == 'ARMATURE':
            msg = "select daz armature first!"
            showInfo(self, context, msg)
            return {'FINISHED'}
            
        pblist = bpy.context.object.pose.bones
        
        for pb in pblist:
            bName = pb.name
            if bName[0] == "r" and bName[1] != ".":
                lbname= "l" + bName[1:]
                for lpb in pblist:
                    lpbName = lpb.name
                    if lpbName == lbname:
                        lpbName = lpbName[:1] + "." + lpbName[1:]
                        lpb.name = lpbName
                        bName = bName[:1] + "." + bName[1:]
                        pb.name=bName
                        
        msg = "rename daz bones for mirror!"
        showInfo(self, context, msg)
        return{'FINISHED'}

class WIF_OT_returnDazBoneName(bpy.types.Operator):
    bl_idname = "bonename.dazname"
    bl_label = "text"
    bl_description = "return bones names as default"
    bl_options = {'UNDO'}
    
    def execute(self, context):
        bpy.ops.dazinfo.reflesh()
        obj = bpy.context.active_object
        if not obj:
            msg = "select renamed armature first!"
            showInfo(self, context, msg)
            return {'FINISHED'}
            
        elif not obj.type == 'ARMATURE':
            msg = "select renamed armature first!"
            showInfo(self, context, msg)
            return {'FINISHED'}
            
        pblist = bpy.context.object.pose.bones
        for pb in pblist:
            bName = pb.name
            if bName[0] == "r" and bName[1] == ".":
                lbname= "l" + bName[1:]
                for lpb in pblist:
                    lpbName = lpb.name
                    if lpbName == lbname:
                        lpbName = lpbName[:1] + lpbName[2:]
                        lpb.name = lpbName
                        bName = bName[:1] + bName[2:]
                        pb.name=bName
                        
        msg = "return daz bone names as default"
        showInfo(self, context, msg)
        return{'FINISHED'}    
    
def read_daz_json(context, fp):
    dazdata = None
    print(fp)
    print(fp[-3:])
    if fp[-3:] == "dbz":
        print ("file is dbz")
        with gzip.open(fp, 'rb') as f:
            bytes = f.read()
            jsdata = bytes.decode("utf-8-sig")
            dazdata = json.loads(jsdata)
            f.close()
            
    elif fp[-4:] == "json":
        with open(fp, 'r', encoding='utf_8_sig') as f:
            print(f.name)
            dazdata = json.load(f)
            f.close()
    return (dazdata)

def convert_daz_json(scn, dazdata):
    amtdic = {}
    if "figures" in dazdata.keys():
        figdic = dazdata["figures"]
    else:
        print("no figure in json")
        scn["wif-info"] = "no figure in json!"         
        return {'FINISHED'}

    figdata = {}
    for fig in figdic:
        if ("bones" in fig.keys() and
            "label" in fig.keys()):
            figdata[fig["label"]] = fig["bones"]
    
    if figdata == {}:
        print ("json have no label!!")
        scn["wif-info"] = "json missing label or bone!"
        return amtdic
    
    for key in figdata.keys(): 
        bonedic = {}
        for bn in figdata[key]:
            bonedic[bn["name"]] = {"cp": bn["center_point"], "ep": bn["end_point"], "rot": bn["ws_rot"]}
        amtdic[key] = bonedic
         
    return amtdic

def set_daz_props(context, rigs, dazdata):
    scn = context.scene
    amtdic = convert_daz_json(scn, dazdata)
    if amtdic == {}:
        return
    for rig in rigs:
        rig.select_set(False)
    for rig in rigs:        
        if rig.type == 'ARMATURE' and "DazRig" in rig.keys():
            sel_act(context, rig)
            bpy.ops.object.mode_set(mode = 'EDIT')
            ebones = rig.data.edit_bones
            sname = rig.name
            
            if "." in sname:
                idx = sname.find(".")
                sname = sname[:idx]            
                        
            rigdic = {}
            if sname in amtdic.keys():
                rigdic = amtdic[sname]
                print("find:", sname)
            else:
                print (sname, " is not in json!!")
                scn["wif-info"] = sname + ":is not in json!!" 
                return

            for bn in ebones:
                bname = bn.name
                if bname in rigdic.keys():
                    bn["DazWsrot"] = rigdic[bname]["rot"]
                    bn["DazCp"] = rigdic[bname]["cp"]
                    bn["DazEp"] = rigdic[bname]["ep"]
                else:
                    print (sname + ": " + bname + " failed!!")
                    scn["wif-info"] = sname + ": " + bname + " failed!!" 
                    continue
                
                if not "def_hd" in bn:
                    bn["def_hd"] = bn.head
                    bn["def_tl"] = bn.tail
                    bn["def_rl"] = bn.roll                

            print(rig.name, ":addprop for bones complete")
            bpy.ops.object.mode_set(mode = 'OBJECT')
    scn["wif-info"] = "Import json succeed!!" 
               
class WIF_OT_SetFilePath(bpy.types.Operator):
    bl_idname = "import_js.setpath"
    bl_label = "setJsonPath"
    bl_description = "Set Json path with select rig"
    bl_options = {'UNDO'}
    
    def execute(self, context):
        bpy.ops.dazinfo.reflesh()
        objs = bpy.context.selected_objects
        scn = bpy.context.scene
        tempId = ""
        for ob in objs:
            if ob.type == "ARMATURE":
                cobs = ob.children            
                if cobs == []:
                    print(ob.name, " no child mesh,  can not set json path for rig")
                    continue
                else:
                    for cob in cobs:
                        if (cob.type == "MESH") and ("DazId" in cob.keys()):
                            tempId = cob.DazId
                            print("find dazmesh:" + tempId)
                            break
                         
            else:
                if "DazId" in ob.keys():
                    tempId = ob.DazId
                else:
                    print(ob.name, " this mesh have no DazID, can not set path")
                    continue        
                
            idx = tempId.rfind("#")
            duf_path = tempId[:idx]
            js_path = duf_path[:-3] + "json"
            dbz_path = duf_path[:-3] + "dbz"
            print(dbz_path)
            
            dir_count = scn.DazNumPaths
            print(dir_count)
            for i in range(dir_count):
                dp = "DazPath" + str(i + 1)
                basePath = getattr(scn, dp)
                
                if os.path.isfile(basePath + duf_path):
                    if not "DazJsAbs" in ob.keys():
                        if os.path.isfile(basePath + dbz_path):
                            ob["DazJsAbs"] = basePath + dbz_path
                        elif os.path.isfile(basePath + js_path):
                            ob["DazJsAbs"] = basePath + js_path
                        else:
                            print("you should export dbz or json!")
                            
                        print(basePath)
                        print(ob.name, "Set Path", ob["DazJsAbs"]) 
                    break
                                                                   
        showInfo(self, context, "Set absolute paths")                    
        
        return {'FINISHED'}


class WIF_OT_ImportDazData(bpy.types.Operator, ImportHelper):
    bl_idname = "import_js.dazdata"
    bl_label = "Import Json"
    bl_description = "import rig data to adjust daz bones"
    bl_options = {'UNDO'}
    
    filter_glob: StringProperty( 
                                default='*.json;*.dbz;*.duf', 
                                options={'HIDDEN'},
                                )
                                    
    directory: StringProperty(
                              name="directory",
                              description="directory used for importing the file",
                              maxlen=1024,
                              )
                              
    filename: StringProperty(
                             name = "filename",
                             description = "filename used for importing the file",
                             )
                             
    info: StringProperty(
                         default = "nomessage",
                         name = "infomsg",
                         description = "infomation",
                        )
    
    def execute(self, context):
        filename, extension = os.path.splitext(self.filepath)
        scn = context.scene
        
        print("directory: ", self.directory)
        print("filename: ", self.filename)
        
        if extension == '.json' or extension == '.dbz':            
            dazdata = read_daz_json(context, self.filepath)
            rigs = bpy.context.selected_objects
            set_daz_props(context, rigs, dazdata)
            
        else:
            msg = "Select json or dbz please!"
            showInfo(self, context, msg)
            
        return {'FINISHED'}
    
    def invoke(self, context, _event):
        scn = context.scene
        bpy.ops.dazinfo.reflesh()
        rigs = bpy.context.selected_objects
        obj = bpy.context.active_object
        
        if not rigs:
            msg = "Select import rigs first!!"
            showInfo(self, context, msg)
            return {'FINISHED'}
        
        if "DazJsAbs" in obj.keys():
            self.filepath = obj["DazJsAbs"]
        else:
            self.filepath = ""
            
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}
        
    #Test to set directory and file for enhancement
    """def invoke(self, context, _event):
        self.filename = ""
        self.directory = ""
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'} """                    
    
def circulate_tp(tcp,tep,ro,length):
    roll = 0
    tp = Vector((0, 0, 0))

    if ro[0] == "X" :       #xyz xzy
        if tep[0] >= tcp[0]:
            tp[0] =  length
            if ro[1] == "Y" :
                roll = pi
            else:
                roll = -pi/2
                
        else:
            tp[0] =  -length
            if ro[1] == "Y" :
                roll = pi
            else:
                roll = pi/2

    elif ro[0] == "Y" :   #yxz, yzx
        if tep[1] >= tcp[1] :
            tp[2] = length
            if ro[1] == "X":
                roll = -pi/2
            else:
                roll = 0

        else:
            tp[2] = -length
            if ro[1] == "X":
                roll =  pi/2
            else:
                roll = 0

    else:
        if tep[2] >= tcp[2] :
            tp[1] = -length
            if ro[1] == "X":
                roll = -pi/2
            else:
                roll = -pi
        else:
            tp[1] =  length
            if ro[1] == "X":
                roll = -pi/2
            else:
                roll = 0

    return tp, roll

def orientate_bone(ebn, ot, pr):
    rx = radians(ot[0])
    ry = radians(ot[1])
    rz = -radians(ot[2])

    dq = (pr[3], -pr[0], pr[2], -pr[1])
    dquat = Quaternion(dq)
    bp_mat = dquat.to_matrix()
    mat_pr = bp_mat.to_4x4()

    mat_o = ebn.matrix
    mat_ra = Matrix.Rotation(rx, 4, 'X')
    mat_rb = Matrix.Rotation(ry, 4, 'Z')
    mat_rc = Matrix.Rotation(rz, 4, 'Y')
    
    mat_r = mat_pr @ mat_rc @ mat_rb @ mat_ra
    ebn.matrix = mat_r @ mat_o

def trans_bone(ebn, cp):
    tcp = Vector((cp[0], -cp[2], cp[1]))
    ebn.translate(tcp)

def generate_ds_bone(amt,cp,ep,tcp,tep,ro,ot,pr,name):
    length = abs((ep - cp).length)
    #print(length)
    bn = amt.data.edit_bones.new(name)
    bn.head = (0, 0, 0)
    tpr = circulate_tp(tcp,tep,ro,length)
    bn.tail = tpr[0]
    bn.roll = tpr[1]
    orientate_bone(bn, ot, pr)
    trans_bone(bn, cp)
    
    for index in range(len(bn.layers)):
        bn.layers[index] = False
        if not index == 16:
            continue
            
        bn.layers[index] = True
        
    

def copy_edit_bone(amt, bname, name):
    ebones = amt.data.edit_bones
    ebones[bname].tail = ebones[name].tail
    ebones[bname].align_orientation(ebones[name])
    
    #comment-out below function to check generate _edit bones
    ebones.remove(ebones[name])

def rtn_edit_bone(amt):
    ebones = amt.data.edit_bones
    for bn in ebones:
        if "def_hd" in bn:
            bn.head = bn["def_hd"]
            bn.tail = bn["def_tl"]
            bn.roll = bn["def_rl"]
            
def memorize_edit_bone(amt):
    ebones = amt.data.edit_bones
    for bn in ebones:
        bn["def_hd"] = bn.head
        bn["def_tl"] = bn.tail
        bn["def_rl"] = bn.roll
        
def update_bn_layer(amt):
    ebones = amt.data.edit_bones
    for ebn in ebones:
        ebn.select = False
    ebn = ebones[0]
    ebn.select = True
    layer = ebn.layers
    bpy.ops.armature.bone_layers(layers=layer)
    
class WIF_OT_adjustDazBonePos(bpy.types.Operator):
    bl_idname = "dazbone.adjust"
    bl_label = "text"
    bl_description = "adjust tip and local axis of selected daz rigs"
    bl_options = {'UNDO'}

    def execute(self, context):
        bpy.ops.dazinfo.reflesh()
        objs = bpy.context.selected_objects
        if not objs:
            msg = "Select daz rigs first!!"
            showInfo(self, context, msg)
            return {'FINISHED'}
            
        for ob in objs:
            ob.select_set(False)
        for ob in objs:
            if ob.type == 'ARMATURE' and "DazRig" in ob.keys():
                sel_act(context, ob)
                #ob.select_set(True)
                #context.view_layer.objects.active = ob
                amt = bpy.context.active_object
                print(amt.name)
                bpy.ops.object.mode_set(mode = 'EDIT')
                ebones = amt.data.edit_bones
                pbones = amt.pose.bones
                bn_lst = []
                for bn in ebones:
                    if "DazHead" in bn.keys() and "DazRotMode" in pbones[bn.name].keys():
                        bn_lst.append(bn.name)
                for bname in bn_lst:
                    if "DazCp" not in ebones[bname].keys():
                        print("Bone without DazCp: %s" % bname)
                        continue

                    name = bname + "_Edt"
                    cp = ebones[bname]["DazHead"]
                    ep = ebones[bname]["DazTail"]
                    ro = pbones[bname]["DazRotMode"]
                    ot = ebones[bname]["DazOrientation"]

                    dcp = ebones[bname]["DazCp"]
                    dep = ebones[bname]["DazEp"]

                    pr = ebones[bname]["DazWsrot"]
                    cp = Vector(cp)/100
                    dcp = Vector(dcp)/100
                    ep = Vector(ep)/100
                    dep = Vector(dep)/100

                    generate_ds_bone(amt,cp,ep,dcp,dep,ro,ot,pr,name)
                    copy_edit_bone(amt, bname, name)
                bpy.ops.object.mode_set(mode = 'OBJECT')
        msg = "Adjust bones Finish!"
        showInfo(self, context, msg)
        return{'FINISHED'}
        
class WIF_OT_memorizeDefBone(bpy.types.Operator):
    bl_idname = "dazbone.memorize"
    bl_label = "text"
    bl_description = "memorize editbones for restore!!"
    bl_options = {'UNDO'}
    
    def execute(self, context):
        bpy.ops.dazinfo.reflesh()
        objs = bpy.context.selected_objects
        
        if not objs:
            msg = "select armatures first!"
            showInfo(self, context, msg)
            return {'FINISHED'}
            
        for ob in objs:
            ob.select_set(False)
            
        for ob in objs:
            if ob.type == 'ARMATURE':
                sel_act(context, ob)
                amt = bpy.context.active_object
                bpy.ops.object.mode_set(mode = 'EDIT')
                memorize_edit_bone(ob)
                bpy.ops.object.mode_set(mode = 'OBJECT')
        
        msg = "Edit-bones memorized!!"
        showInfo(self, context, msg)
        return{'FINISHED'}                
        
class WIF_OT_returnDefBone(bpy.types.Operator):
    bl_idname = "dazbone.return"
    bl_label = "text"
    bl_description = "return editbones to memorized"
    bl_options = {'UNDO'}
    
    def execute(self, context):
        bpy.ops.dazinfo.reflesh()
        objs = bpy.context.selected_objects
        
        if not objs:
            msg = "select adjusted rig first!"
            showInfo(self, context, msg)
            return {'FINISHED'}
            
        for ob in objs:
            ob.select_set(False)
            
        for ob in objs:
            if ob.type == 'ARMATURE':
                sel_act(context, ob)
                amt = bpy.context.active_object
                bpy.ops.object.mode_set(mode = 'EDIT')
                rtn_edit_bone(ob)
                bpy.ops.object.mode_set(mode = 'OBJECT')
                
        msg = "Edit-bones restored!!"
        showInfo(self, context, msg)
        return{'FINISHED'}
    
class WIF_OT_transferCustomprops(bpy.types.Operator):
    bl_idname = "dazbone.transferprop"
    bl_label = "text"
    bl_description = "transfer customprops from active rig to selected rig"
    bl_options = {'UNDO'}
    
    def execute(self, context):        
        objs = bpy.context.selected_objects
        print (objs)
        
        d_obj = bpy.context.active_object
        print (d_obj)
        
        objs.remove(d_obj)        
        
        if not objs:
            msg = "select traget rigs, next select donnar rig"
            showInfo(self, context, msg)
            return {'FINISHED'}
        
        elif not d_obj.type == "ARMATURE":
            msg = "select traget rigs, next select donnar rig"
            showInfo(self, context, msg)
            return {'FINISHED'}
        
        d_bones = d_obj.data.bones
        
        for ob in objs:            
            if not ob.type == "ARMATURE":
                continue
            t_bones = ob.data.bones            
            for bn in t_bones.keys():
                if bn in d_bones.keys():
                    if not "def_hd" in t_bones[bn].keys():
                        t_bones[bn]["def_hd"] = d_bones[bn]["def_hd"]
                    if not "def_rl" in t_bones[bn].keys():
                        t_bones[bn]["def_rl"] = d_bones[bn]["def_rl"]
                    if not "def_tl" in t_bones[bn].keys():
                        t_bones[bn]["def_tl"] = d_bones[bn]["def_tl"]                    
                    
                    # for prop in d_bones[bn].keys():
                       # if prop in t_bones[bn].keys():
                            # continue
                        # t_bones[bn][prop] = d_bones[bn][prop]       
                
        msg = "Edit-bones restored!!"
        showInfo(self, context, msg)
        return{'FINISHED'}

class WIF_OT_showBoneDots(bpy.types.Operator):
    bl_idname = "dazbone.dotshow"
    bl_label = "text"
    bl_description = "update bone-layer to show all dots"
    bl_options = {'UNDO'}

    def execute(self, context):
        bpy.ops.dazinfo.reflesh()
        objs = bpy.context.selected_objects
        if not objs:
            msg = "select armatures first!"
            showInfo(self, context, msg)
            return {'FINISHED'}
            
        for ob in objs:
            ob.select_set(False)
            
        for ob in objs:
            if ob.type == 'ARMATURE':
                sel_act(context, ob)
                amt = bpy.context.active_object
                bpy.ops.object.mode_set(mode = 'EDIT')
                update_bn_layer(ob)
                bpy.ops.object.mode_set(mode = 'OBJECT')
                
        msg = "up-date all bone layers!!"
        showInfo(self, context, msg)
        return{'FINISHED'}            

def select_children(self, context, mode):
    if mode == 'POSE':
        bn_selection = bpy.context.selected_pose_bones        
    else:
        bn_selection = bpy.context.selected_editable_bones
    
    b_list = []
      
    if not len(bn_selection)==0:
        for i in bn_selection:
            b_list += i.children_recursive
            b_list = list(set(b_list))
            
        for i in b_list:
            if mode == 'POSE':
                i.bone.select = True
            else:
                i.select = True
                i.select_head = True
                i.select_tail = True
                
        msg = "child bones selected!!"
        showInfo(self, context, msg)
                
    else:
        msg = "select bones first!!"
        showInfo(self, context, msg)
    
    return
    
def unselect_children(self, context, mode):

    if mode == 'POSE':
        bn_selection = bpy.context.selected_pose_bones
        bn_active = bpy.context.active_pose_bone
    else:
        bn_selection = bpy.context.selected_bones
        bn_active = bpy.context.active_bone
        
    b_list = []
        
    if bn_active:
        print("active bone ", bn_active)        
        b_list = bn_active.children_recursive
        b_list.append(bn_active)
        if mode == 'POSE':
            for i in b_list:                
                i.bone.select = False
        else:
            for i in b_list:
                i.select_head = False
                i.select_tail = False
                i.select = False
        msg = "child bones unselected!!"
        showInfo(self, context, msg)
    else:
        msg = "select bones first!!"
        showInfo(self, context, msg)
    
    return
    
class WIF_OT_SelectChildBones(bpy.types.Operator):
    bl_idname = "dazbone.selectchild"
    bl_label = "SelectChildBones"
    bl_description = "select child bones from current selection (for pose and edit mode)"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        bpy.ops.dazinfo.reflesh()
        aob = bpy.context.active_object
        if not aob:
            msg = "select bones in pose or edit mode"
            showInfo(self, context, msg)
            return {'FINISHED'}
            
        elif not aob.type == "ARMATURE":
            msg = "select bones in pose or edit mode"
            showInfo(self, context, msg)
            return {'FINISHED'}
            
        elif aob.mode == 'OBJECT':
            msg = "select bones in pose or edit mode"
            showInfo(self, context, msg)
            return {'FINISHED'}
            
        else:
            select_children(self, context, aob.mode)
            
        return {'FINISHED'}
        
class WIF_OT_DeselectChildBones(bpy.types.Operator):
    bl_idname = "dazbone.deselectchild"
    bl_label = "DeselectChildBones"
    bl_description = "deselect child bones of Active bone (for pose and edit mode)"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        bpy.ops.dazinfo.reflesh()
        aob = bpy.context.active_object
        if not aob:
            msg = "select bones in pose or edit mode"
            showInfo(self, context, msg)
            return {'FINISHED'}
            
        elif not aob.type == "ARMATURE":
            msg = "select bones in pose or edit mode"
            showInfo(self, context, msg)
            return {'FINISHED'}
            
        elif aob.mode == 'OBJECT':
            msg = "select bones in pose or edit mode"
            showInfo(self, context, msg)
            return {'FINISHED'}  
            
        else:
            unselect_children(self, context, aob.mode)

        return {'FINISHED'}
        
class WIF_OT_flipBones(bpy.types.Operator):
    bl_idname = "dazbone.flipbone"
    bl_label = "text"
    bl_description = "flip selected edit-bones along bone direciton"
    bl_options = {'UNDO'}

    def execute(self, context):
        bpy.ops.dazinfo.reflesh()
        ob = bpy.context.active_object
        slist = []
        if not ob:
            msg = "select bones in edit mode!!"
            showInfo(self, context, msg)
            return{'FINISHED'}
            
        if ob.type == "ARMATURE" and ob.mode == "EDIT":
            ebn = bpy.context.selected_bones
            for bn in ebn:
                slist.append(bn.name)
            print(slist)
            for bn in slist:
                ebone = ob.data.edit_bones[bn]
                ehead = ebone.head
                etail = ebone.tail
                ebone.tail = ehead + ehead - etail
            
            msg = "flip bones!!"
            showInfo(self, context, msg)
            return{'FINISHED'}
        
        else:
            msg = "select bones in edit mode!!"
            showInfo(self, context, msg)
            return{'FINISHED'}
        
        return{'FINISHED'}                

class WIF_OT_addRoll(bpy.types.Operator):
    bl_idname = "dazbone.addroll"
    bl_label = "text"
    bl_description = "plus or minus roll 90 for selected edit bones"
    bl_options = {'UNDO'}
    
    rfg: BoolProperty(default=True)

    def execute(self, context):
        ob = bpy.context.active_object
        bpy.ops.dazinfo.reflesh()
        if not ob:
            msg = "select bones in edit mode!"
            showInfo(self, context, msg)
            return{'FINISHED'}
            
        if ob.type == 'ARMATURE' and context.mode == 'EDIT_ARMATURE':
            ebones = context.selected_bones
            if self.rfg == True:
                for bn in ebones:
                    bn.roll += pi/4
            else:
                for bn in ebones:
                    bn.roll -= pi/4
            msg = "change roll!!"
            showInfo(self, context, msg)
            return{'FINISHED'}
            
        else:
            msg = "select bones in edit mode!"
            showInfo(self, context, msg)
            return{'FINISHED'}

def save_presets(context, filepath, data):
    with open(filepath, 'w', encoding ='utf-8') as f:
        json.dump(data, f, indent = 4)
        f.close()
    return{'FINISHED'}

class WIF_OT_saveDazMorph(bpy.types.Operator, ExportHelper):
    bl_idname = "export.morph_preset"
    bl_label = "Export Morph preset"
    bl_description = "Export morphs value as json"
    bl_options = {'UNDO'}
    
    filename_ext = ".json"
    filter_glob: StringProperty(
            default = "*.json",
            options = {'HIDDEN'},
            )
            
    # use_setting: BoolProperty(
    #   name="Use preset directory",
    #   description="open preset folda",
    #   default=False,
    #)
            
    morph_dic = {}
    
    def make_preset_dic(self, context):
        act = bpy.context.active_object
        self.morph_dic = {}
        self.morph_dic = import_daz.getMorphs(act, "All")
        print (self.morph_dic)
        #for prop in act.keys():
        #    if prop[0: 2] == "Dz":
        #       self.morph_dic[prop] = round(act[prop], 2)
        
    def execute(self, context):
        dic = self.morph_dic
        save_presets(context, self.filepath, dic)
        showInfo(self, context, ("saved preset " + self.filepath))
        return{'FINISHED'} 
    
    def invoke(self, context, _event):
        bl_path = bpy.data.filepath        
        dir = os.path.dirname(bl_path)
        dispname = bpy.path.display_name_from_filepath(bl_path)
        jsname = dispname + "_preset" + ".json"      
        self.make_preset_dic(context)       
        
        context.window_manager.fileselect_add(self)
        self.filepath = os.path.join(dir, jsname)
        
        return{'RUNNING_MODAL'}
    
# import saved morph preset json

class WIF_OT_loadDazMorph(bpy.types.Operator, ImportHelper):
    bl_idname = "import.morph_preset"
    bl_label = "Import Morph preset"
    bl_description = "Import morph preset"
    bl_options = {'UNDO'}
    
    filename_ext = ".json"
    filename = "preset.json"

    filter_glob: StringProperty(
            default = "*.json; *.blend",
            options = {'HIDDEN'},
            )
            
    morph_dic = {}
    
    """def import_mpreset(self, context, filepath):
        with open(filepath, 'r', encoding ='utf-8') as f:
            self.morph_dic = json.load(f)
            f.close()"""
            
    def set_morphs (self, context, dic):
        act = bpy.context.active_object
        for prop in dic.keys():
            if prop in act:
                act[prop] = dic[prop]
        update_prop(context, act)
                

    def execute(self, context):
        self.morph_dic = read_daz_json(context, self.filepath)
        print(self.morph_dic)
        
        self.set_morphs(context, self.morph_dic)
        dispname = bpy.path.display_name_from_filepath(self.filepath)
        msg = dispname + " loaded !!"
        showInfo(self, context, msg)
        
        return{'FINISHED'}
        
    
    def invoke(self, context, _event):
        bl_path = bpy.data.filepath
        if bl_path == "":
            msg = "plase save blend file first!!"
            showInfo(self, context, msg)
            return{'FINISHED'}
        
        dir = os.path.dirname(bl_path)
        dispname = bpy.path.display_name_from_filepath(bl_path)
        jsname = dispname + "_preset" + ".json"                   
        
        context.window_manager.fileselect_add(self)
        
        self.filepath = os.path.join(dir, jsname)     
        return{'RUNNING_MODAL'}
    
class WIF_OT_saveRigPreset(bpy.types.Operator, ExportHelper):
    bl_idname = "export.rig_set"
    bl_label = "Export rig preset"
    bl_description = "Export rig preset as json"
    bl_options = {'UNDO'}
    
    filename_ext = ".json"
    
    filter_glob: StringProperty(
            default = "*.json",
            options = {'HIDDEN'},
            )
            
    rig_dic = {}
            
    def make_rigset_dic(self, context):
        rigs = bpy.context.selected_objects
        self.rig_dic = {}
        
        for rig in rigs:
            context.view_layer.objects.active = rig
            bpy.ops.object.mode_set(mode='EDIT')
            bone_dic = {}
            print(rig.name)
            ebones = rig.data.edit_bones
            
            for bn in ebones:                
                print(bn)
                prop_dic =  {}
                prop_dic["rl"] = bn.roll
                prop_dic["hd"] = (bn.head[0], bn.head[1], bn.head[2])
                prop_dic["tl"] = (bn.tail[0], bn.tail[1], bn.tail[2])
                
                bone_dic[bn.name] = prop_dic
                 
            self.rig_dic[rig.name] = bone_dic
            bpy.ops.object.mode_set(mode='OBJECT')        
        
    def execute(self, context):
        
        dic = self.rig_dic
        save_presets(context, self.filepath, dic)
        showInfo(self, context, "export rig preset!!")
        return{'FINISHED'} 
    
    def invoke(self, context, _event):
        self.make_rigset_dic(context)
        bl_path = bpy.data.filepath        
        dir = os.path.dirname(bl_path)  
        
        dispname = bpy.path.display_name_from_filepath(bl_path)
        jsname = dispname + "_rigset" + ".json"
        print(jsname)
                
        context.window_manager.fileselect_add(self)       
        self.filepath = os.path.join(dir, jsname)      
        showInfo(self, context, self.filepath)
          
        return{'RUNNING_MODAL'}          
        
    
class WIF_OT_loadRigPreset(bpy.types.Operator, ImportHelper):
    bl_idname = "import.rig_set"
    bl_label = "load rig set"
    bl_description = "Import rig preset"
    bl_options = {'UNDO'}
    
    filename_ext = ".json"
    filename = "preset.json"

    filter_glob: StringProperty(
            default = "*.json; *.blend",
            options = {'HIDDEN'},
            )
            
    rig_dic = {}    
            
    def set_rigs(self, context, dic):
        rigs = bpy.context.selected_objects       
        for rig in rigs:
            if rig.name in dic.keys():
                bone_dic = {}
                print(rig.name)
                bone_dic = dic[rig.name]               
                context.view_layer.objects.active = rig
                bpy.ops.object.mode_set(mode='EDIT')
                ebones = rig.data.edit_bones
                for bn in ebones:
                    if bn.name in bone_dic.keys():
                        print(bn.name)
                        prop_dic = []
                        prop_dic = bone_dic[bn.name]
                                            
                        bn.roll = prop_dic["rl"]
                        bn.head = prop_dic["hd"]
                        bn.tail = prop_dic["tl"]
                bpy.ops.object.mode_set(mode='OBJECT')
            
            else:
                print("can not find ", rig.name, "in json")
                continue                    

    def execute(self, context):
        self.rig_dic = read_daz_json(context, self.filepath)        
        self.set_rigs(context, self.rig_dic)
        dispname = bpy.path.display_name_from_filepath(self.filepath)
        msg = dispname + " loaded !!"
        showInfo(self, context, msg)
        
        return{'FINISHED'}        
    
    def invoke(self, context, _event):
        bl_path = bpy.data.filepath        
        dir = os.path.dirname(bl_path)
        path = pathlib.Path(dir + "\preset")        
        dispname = bpy.path.display_name_from_filepath(bl_path)
        print(dispname)
        jsname = dispname + "_rigset" + ".json"                  
        
        context.window_manager.fileselect_add(self)        
        filepath = os.path.join(dir, jsname)
        self.filepath = filepath   
        return{'RUNNING_MODAL'}
    
class WIF_OT_addRollpreset(bpy.types.Operator, ImportHelper):
    bl_idname = "import.roll_set"
    bl_label = "import roll preset"
    bl_description = "Do not use after add driver!! Import roll template then adust roll"
    bl_options = {'UNDO'}
    
    filename_ext = ".json"
    filename = "preset.json"

    filter_glob: StringProperty(
            default = "*.json; *.blend",
            options = {'HIDDEN'},
            )
            
    roll_dic = {}    
     
    def add_rolls(self, context, dic):
        rigs = bpy.context.selected_objects       
        for rig in rigs:
            if rig.DazRig in dic.keys():
                print(rig.DazRig)             
                context.view_layer.objects.active = rig
                bpy.ops.object.mode_set(mode='EDIT')
                ebones = rig.data.edit_bones
                bone_dic = {}
                bone_dic = dic[rig.DazRig]
                for bn in ebones:
                    if bn.name in bone_dic.keys():                                            
                        bn.roll += bone_dic[bn.name]*pi/180
                        print(bn.name, "add roll")                        
                bpy.ops.object.mode_set(mode='OBJECT')
            
            else:
                print("can not find ", rig.DazRig, "in template")
                continue                    

    def execute(self, context):
        self.roll_dic = read_daz_json(context, self.filepath)        
        self.add_rolls(context, self.roll_dic)
        
        dispname = bpy.path.display_name_from_filepath(self.filepath)
        msg = dispname + "add rolls from roll template !!"
        showInfo(self, context, msg)
        
        return{'FINISHED'}
        
    
    def invoke(self, context, _event):
        bl_path = bpy.data.filepath        
        dir = os.path.dirname(bl_path)        
        dispname = bpy.path.display_name_from_filepath(bl_path)
        print(dispname)
        jsname = dispname + "_rollset" + ".json"                  
        
        context.window_manager.fileselect_add(self)        
        filepath = os.path.join(dir, jsname)
        self.filepath = filepath   
        return{'RUNNING_MODAL'}
    
classes = (
    WIF_OT_addinfo,
    WIF_PT_dazBoneUtils,
    WIF_OT_changeDazBoneName,
    WIF_OT_returnDazBoneName,
    WIF_OT_SetFilePath,
    WIF_OT_ImportDazData,
    WIF_OT_adjustDazBonePos,
    WIF_OT_memorizeDefBone,
    WIF_OT_returnDefBone,
    WIF_OT_transferCustomprops,
    WIF_OT_showBoneDots,
    WIF_OT_SelectChildBones,
    WIF_OT_DeselectChildBones,
    WIF_OT_flipBones,
    WIF_OT_addRoll,
    WIF_OT_saveDazMorph,
    WIF_OT_loadDazMorph,
    WIF_OT_saveRigPreset,
    WIF_OT_loadRigPreset,
    WIF_OT_addRollpreset
)

def register():
    for cls in classes:
        bpy.utils.register_class(cls)
    
def unregister():
    unsetSceneprop()
    for cls in classes:
        bpy.utils.unregister_class(cls)

if __name__ == "__main__":
    register()